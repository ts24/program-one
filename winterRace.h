#pragma once
#include <iostream>
#include <string>



class WinterRace {
private:
//I don't know if there should be more data that we want to store here.
    std::string name;
    int age;
    int id;
    int wins;

public:
    WinterRace();
    WinterRace(std::string racer,int id, int wins);
    ~WinterRace();
    int randomNumber(); // generates a random number to determine the winner 

    //I put these here because all of the types of races will use this even though they are different races
    void chooseRace(); // I think this should return an object but im not sure
    void determineWinner(); // Not really sure which return type to use here. I might make it return an object so it can display the name and maybe how many seconds they won by.
    void startRace(); //begins race for whichever event
    ~WinterRace();
};